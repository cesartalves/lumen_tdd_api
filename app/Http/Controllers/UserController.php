<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class UserController extends Controller
{  
    public function view($id){
        return User::find($id);
    }

    public function list(){
        return User::all();
    }

    public function login(Request $request){

        $data = $request->only('email', 'password');
   
        $user = User::where('email', $data['email'])
                ->first();

        try {

            if(Crypt::decrypt($user->password) == ($data['password'])){

                $user->api_token = str_random(60);
                $user->update();
                return ['api_token' => $user->api_token];

            }
            else return response()->json(['error' => 'Invalid password'], 401);

        } catch (DecryptException $e) {
            
            //return response()->json(['error' => 'Unauthorized'], 401);
        }        
    }

    public function store(Request $request){

        $this->validate($request, 
            [
                'email' => 'required|unique:users|max:255', 
                'name' => 'required',
                'password' => 'required|confirmed'  
            ]);

        $user = new User($request->all());
        $user->password = Crypt::encrypt($request->input('password'));
        $user->api_token = str_random(60);
        $user->save();

        return $user; 
    }

    public function update(Request $request, $user_id){

        $validationData = array( 'email' => 'required|unique:users|max:255', 
                'name' => 'required');

        if(isset($request->all()['password']))    
            $validatinData['password'] = 'required|confirmed';
        
        $this->validate($request, 
           $validationData);
        
        $user = User::find($user_id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');

        if(isset($request->all()['password'])) 
            $user->password = Crypt::encrypt($request->input('password'));

        $user->update();
        return $user; 
    }

    public function delete(Request $request, $user_id){

        $user = User::find($user_id);

        if($user->delete())
            return new Response('', 200);
        else return new Response('', 401);
        
    }
}
