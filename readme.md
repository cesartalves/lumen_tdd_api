# COMMANDS

composer create-project --prefer-dist laravel/lumen lumen-tdd
php -S localhost:8000 -t public

vendor/phpunit/phpunit/phpunit

mysql { 

    mysql -u root -p
    create database lumen_api;

}

php artisan migrate

php artisan make:migration create_table_user --create users
php artisan make:migration add_api_token_users --table users

php artisan migrate


extras

php artisan make:seed UserTableSeeder

php artisan migrate:fresh --seed

