<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

use App\User;

class UserTest extends TestCase
{

    use DatabaseTransactions; //php trait
 
    private function userDataFromFactory(){
        $user = factory(App\User::class)->make();
        
        return $data = [
            'name' => $user->name,
            'email' => $user->email,
            'password' => $user->password,
            'password_confirmation' => $user->password
        ];
    }

    public function testAll(){

        $this->actingAs(User::where('api_token', '<>', '')
            ->first())
            ->get("/api/users/");

        $this->assertResponseOk();

        $response = json_decode($this->response->content());

        $this->seeJsonStructure(
            [
              '*' => ['id', 'name', 'email']      
            ]
        );
    }

    public function testViewUser(){

        $user = User::where('api_token', '<>', '')->first();

        $this->actingAs($user)->get("/api/user/$user->id");

        $this->assertResponseOk();

        $response = (array) json_decode($this->response->content());

        $this->assertArrayHasKey('name', $response);
        $this->assertArrayHasKey('email', $response);
        $this->assertArrayHasKey('id', $response);

    }

    public function testCreateUser()
    {
        $data = $this->userDataFromFactory();

        $this->post('/api/user', $data);
        $this->assertResponseOk();

        $response = (array) json_decode($this->response->content());

        $this->assertArrayHasKey('name', $response);
        $this->assertArrayHasKey('email', $response);
        $this->assertArrayHasKey('id', $response);

        $this->seeInDatabase('users', [

                'name' => $data['name'],
                'email' => $data['email'],
                //password left out for crypt reasons. it's empty ?
            ]);
    }

    public function testUpdateUserWithoutPassword()
    {
        $data = [
            'name' => 'modified',
            'email' => 'modified@example.com',
        ];

       $this->updateUserAssertions($data);
    }

    public function testUpdateUserWithPassword()
    {
        $data = $this->userDataFromFactory();

        $this->updateUserAssertions($data);     
    }

    private function updateUserAssertions($data){

        $user = User::where('api_token', '<>', '')->first();

        $this->actingAs($user)->put("/api/user/$user->id", $data);
        $this->assertResponseOk();

        $response = (array) json_decode($this->response->content());

        $this->assertArrayHasKey('name', $response);
        $this->assertArrayHasKey('email', $response);
        $this->assertArrayHasKey('id', $response);

        // $this->assertEquals('modified', $response['name']);
        // $this->assertEquals('modified@example.com', $response['email']);

        // $this->assertEquals('modified', User::find($user->id)->password);

        $this->seeInDatabase('users', [

                'name' => $data['name'],
                'email' => $data['email'],
            ]);

        $this->notSeeInDatabase('users', [

            'name' => $user->name,
            'email' => $user->email,
            'id' => $user->id
        ]);
    }
    
    public function testDelete(){

        $user = User::first();

        $request = $this->actingAs($user)->delete("/api/user/$user->id");

        $this->assertResponseOk();

        $user = User::withTrashed()->find($user->id);

        $this->assertTrue($user->trashed());

        $this->notSeeInDatabase('users', 
            [ 'id' => $user->id,
               'deleted_at' => null     
            ]      
        );
    }

    public function testLogin(){

        $data = $this->userDataFromFactory();

        $this->post('/api/user', $data);
        $this->assertResponseOk();

        $this->post('/login', $data);
        $this->assertResponseOk();

        $response = (array) json_decode($this->response->content());

        $this->assertArrayHasKey('api_token', $response);
    }
}
